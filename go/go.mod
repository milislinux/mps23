module mpsd

go 1.19

require gopkg.in/ini.v1 v1.67.0

require (
	github.com/cavaliergopher/grab/v3 v3.0.1 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
)
